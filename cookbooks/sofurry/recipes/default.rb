pkgs = %w{ php5-fpm php5-mysql php5-imagick php5-curl php5-memcached phpmyadmin }

pkgs.each do |pkg|
  package pkg do
    action :install
  end
end